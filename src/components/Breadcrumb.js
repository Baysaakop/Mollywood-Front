import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Breadcrumbs } from '@material-ui/core';
import Link from '@material-ui/core/Link';

const useStyles = makeStyles((theme) => ({
    container: {
        backgroundColor: theme.palette.breadcrumb,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        minHeight: "200px",
    },
    breadcrumb: {         
        display: "flex",   
        justifyContent: "center",
    },
    link: {
        fontSize: "12px",
    },
}));

const Breadcrumb = (props) => {
    const classes = useStyles();
    return (
        <div className={classes.container}>
            <div> 
                <Typography color="textPrimary" style={{ fontSize: "32px" }}>{props.page}</Typography>
                <Breadcrumbs className={classes.breadcrumb} aria-label="breadcrumb"> 
                    <Link color="inherit" href="/">
                        <Typography className={classes.link}>MOLLYWOOD</Typography> 
                    </Link>
                    <Typography color="textPrimary" className={classes.link}>{props.page}</Typography>
                </Breadcrumbs>
            </div>
        </div>
    );
};

export default Breadcrumb;